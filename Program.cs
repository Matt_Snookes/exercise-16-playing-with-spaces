﻿using System;

namespace exercise_16
{
    class Program
    {
        static void Main(string[] args)
        {
            //Start the program with Clear();
            Console.Clear();
            var name="";
            System.Console.WriteLine("\t\t***************************************************");
            System.Console.WriteLine("\n\t\t***************************************************");
            System.Console.WriteLine("\n\t\t*************  Welcome to my app  *****************");
            System.Console.WriteLine("\n\t\t***************************************************");
            System.Console.WriteLine("\n\t\t**********   Please enter your name  **************");
            System.Console.WriteLine("\n\t\t***************************************************");
            name = Console.ReadLine();
            System.Console.WriteLine($"\n\t\t***\tHello {name} Thanks for trying my app\t***");
            System.Console.WriteLine("\n\t\t***************************************************");
            
            //End the program with blank line and instructions
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("Press <Enter> to quit the program");
            Console.ReadKey();
        }
    }
}
